function setCookie(name, value, days) {
    var expires = ""
    if (days) {
        var date = new Date()
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
        expires = "; expires=" + date.toUTCString()
    }
    document.cookie = name + "=" + (value || "") + expires + ", Path=/m SameSite=Strictm";
}

function getCookie(name) {
    var nameEQ = name + "="
    var ca = document.cookie.split(';')
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length)
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length)
        }
    }
    return undefined;
}

function documentReady(fn) {
    if (document.readyState !== 'loading') {
        fn()
    } else {
        document.addEventListener('DOMContentLoaded', fn)
    }
}

if (!getCookie('cookie-consent-tracking-allowed')) {
  documentReady(function () {
    $('body').addClass('overlay');
    $(".cookies-container").css({ display: "block" });
  }.bind(this));
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}


$(document).ready(function () {
  $('.hamburger.hamburger--spin').on('click touch', function() {
    $(this).toggleClass('is-active');
    $('.top-header__holder.mobile').toggleClass('hidden');
  });

  $(".js-closer-icon, .js-closer-button").on("click", function (e) {
    e.preventDefault();
    setCookie('cookie-consent-tracking-allowed', 'true', 365)
    $('body').removeClass('overlay');
    $(".cookies-container").css({ display: "none" });
  });

  $('.desktop-header .top-header__menu .dropdown[hover]').mouseover(function() {
    $(this).find('.dropdown-toggle').addClass('rotate');
    $(this).find('.dropdown-menu').css({
      display: 'block',
      opacity: '1',
      'box-shadow': 'rgba(0, 0, 0, 0,3) 0px 20px 20px 0px',
      'transform': 'translate(0px, 0px)'
    });
  });

  $('.desktop-header .top-header__menu .dropdown[hover]').mouseleave(function() {
    $(this).find('.dropdown-toggle').removeClass('rotate');
    $(this).find('.dropdown-menu').css({
      display: 'block',
      'box-shadow': 'none',
      transform: 'translate(0px, -100%)',
      'opacity': '0'
    });
  });

  $('.mobile-header .top-header__menu .dropdown .dropdown-toggle').on('click touch', function() {
    $(this).toggleClass('rotate');
    $(this).siblings('.dropdown-menu').css({display: $(this).hasClass('rotate') ? 'block': 'none'});
  });

  $('.js-mobile-child-dropdown').on('click touch', function() {
    $('.mobile-header .hamburger').removeClass('is-active');
    $('.mobile-header .top-header__menu .dropdown .dropdown-toggle').removeClass('rotate');
    $('.mobile-header .top-header__menu .dropdown .dropdown-toggle').siblings('.dropdown-menu').css({display:  'none'});
    $('.mobile-header .top-header__holder').addClass('hidden');
  });

  $('.js-join-news').on('click', function(e) {
    e.preventDefault();

    $('body').addClass('overlay');
    $(".subscribe-container").css({ display: "block" });
  });

  $('.js-subscribe-close').on('click', function(e) {
    e.preventDefault();

    $('body').removeClass('overlay');
    $(".subscribe-container").css({ display: 'none' });
    $('#exampleInputPassword1').removeClass('is-invalid');
    $('#exampleInputEmail1').removeClass('is-invalid');

    $('#exampleInputPassword1').val('');
    $('#exampleInputEmail1').val('');

    $('.js-sub-fields').css({display: 'block'});
    $('.js-response-sub').css({display: 'none'});

    $('#emailHelp').html("We'll never share your email with anyone else.");
  });

  $('#exampleInputPassword1, #exampleInputEmail1').on('focusin', function(){
    $(this).removeClass('is-invalid');
  });

  $('.subscribe-button').on('click', function() {
    const name = $('#exampleInputPassword1').val();
    const email = $('#exampleInputEmail1').val();

    if (!name) {
      $('#exampleInputPassword1').addClass('is-invalid');
    }
    if (!isEmail(email)) {
      $('#exampleInputEmail1').addClass('is-invalid');
      return;
    }

    if (name && email) {
      $.getJSON('https://somv.us15.list-manage.com/subscribe/post-json?' + 
            'u=24045a65fbf3c7eeae33cd61f&amp;id=ee139fda0e'+
            '&NAME='+ name +
            '&EMAIL=' + email +
            '&b_24045a65fbf3c7eeae33cd61f_ee139fda0e=&c=?', function(json) {
              if (json) {
                if (json.result === 'success') {
                  $('.js-sub-fields').css({display: 'none'});
                  $('.js-response-sub').css({display: 'block'});
                }

                if (json.result === 'error') {
                  $('#emailHelp').html(json.msg);
                }
              }
            });
    }
  });
});
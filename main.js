const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();


router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
});

router.get('/companies',function(req,res){
  res.sendFile(path.join(__dirname+'/companies.html'));
});

router.get('/team/yuval-baharav',function(req,res){
  res.sendFile(path.join(__dirname+'/yuval-baharav.html'));
});

router.get('/team/pinhas-buchris',function(req,res){
  res.sendFile(path.join(__dirname+'/pinhas-buchris.html'));
});

router.get('/team/nir-adler',function(req,res){
  res.sendFile(path.join(__dirname+'/nir-adler.html'));
});

router.get('/team/chen-eilom',function(req,res){
  res.sendFile(path.join(__dirname+'/chen-eilom.html'));
});

router.get('/team/shelly-nice',function(req,res){
  res.sendFile(path.join(__dirname+'/shelly-nice.html'));
});

router.get('/team/andy-bellass',function(req,res){
  res.sendFile(path.join(__dirname+'/andy-bellass.html'));
});

router.get('/team/merav-rotem-naaman',function(req,res){
  res.sendFile(path.join(__dirname+'/merav-rotem-naaman.html'));
});

router.get('/about',function(req,res){
  res.sendFile(path.join(__dirname+'/about.html'));
});

router.get('/contact',function(req,res){
  res.sendFile(path.join(__dirname+'/contact.html'));
});

router.get('/terms-of-use',function(req,res){
  res.sendFile(path.join(__dirname+'/terms-of-use.html'));
});

router.get('/privacy-policy',function(req,res){
  res.sendFile(path.join(__dirname+'/privacy-policy.html'));
});

router.get('/cookie-policy',function(req,res){
  res.sendFile(path.join(__dirname+'/cookie-policy.html'));
});

router.get('/lp-central',function(req,res){
  res.sendFile(path.join(__dirname+'/lp-central.html'));
});

app.use(express.static(__dirname));

//add the router
app.use('/', router);

app.listen(process.env.port || 4200);

console.log('Running at Port 4200');